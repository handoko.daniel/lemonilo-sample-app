import React, { Component } from 'react';
import { View, Text, StyleSheet, Dimensions } from 'react-native';
import { TouchableOpacity } from 'react-native-gesture-handler';
import Icon from 'react-native-vector-icons/Ionicons';

// import custom component
import Constant from '../../utilities/Constant';

export default class StaticHeader extends Component {
    constructor(props) {
        super(props);
        this.state = {
        };
    }

    renderHeader() {
        const { type } = this.props;

        if (type == "default") {
            return (
                <View style={[localStyles.container, { flexDirection: 'row', alignItems: 'center' }]}>
                    <TouchableOpacity
                        onPress={this.props.onPress}
                        style={localStyles.searchArea}>
                        <Icon
                            name="ios-search"
                            size={24}
                            color={Constant.Color.THIN_GRAY}
                            style={{ marginRight: 8 }}
                        />
                        <Text style={localStyles.placeholderLabel}>Search..</Text>
                    </TouchableOpacity>
                    <TouchableOpacity
                        onPress={this.props.onCartPressed}
                        style={{paddingLeft: 16}}
                    >
                        <Icon
                            name="ios-cart"
                            size={24}
                            color={Constant.Color.ABSOLUTE_WHITE}
                        />
                    </TouchableOpacity>
                </View>
            )
        }
        else if (type == "Back") {
            return (
                <View style={localStyles.container}>
                    <TouchableOpacity
                        onPress={this.props.onPress}>
                        <Icon
                            name="ios-chevron-back"
                            size={24}
                            color={Constant.Color.ABSOLUTE_WHITE}
                        />
                    </TouchableOpacity>
                </View>
            )
        }
        else {
            return (
                <View style={localStyles.container} />
            )
        }
    }

    render() {
        return (
            this.renderHeader()
        );
    }
}

const localStyles = StyleSheet.create({
    container: {
        width: "100%",
        padding: 16,
        backgroundColor: Constant.Color.PRIMARY
    },
    searchArea: {
        width: Dimensions.get('window').width * 0.8,
        flexDirection: 'row',
        alignItems: "center",
        padding: 8,
        backgroundColor: Constant.Color.ABSOLUTE_WHITE,
        borderRadius: 24,
        paddingHorizontal: 16
    },
    placeholderLabel: {
        fontSize: 14,
        color: Constant.Color.THIN_GRAY
    }
})