import {
    USER_CART,
    SET_INITIAL_VALUE
} from './actions';

const initialState = {
    cart: [],
}

export default (state = initialState, action) => {
    const { type, payload } = action;

    switch (type) {
        case USER_CART:
            return {
                ...state,
                cart: payload
            }
        case SET_INITIAL_VALUE:
            return {
                ...state,
                cart: initialState.cart,

            }
        default:
            return state;
    }
};
