export const USER_CART = 'USER_CART';
export const SET_INITIAL_VALUE = 'SET_INITIAL_VALUE';

export function setInitialValue(payload) {
    return function (dispatch) {
        dispatch({
            type: SET_INITIAL_VALUE,
            payload
        });
    }
}

export function addToCart(payload) {
    return function (dispatch) {
        dispatch({
            type: USER_CART,
            payload
        });
    }
}