import * as React from 'react';
import { Text, View } from 'react-native';
import { NavigationContainer } from '@react-navigation/native';
import { createBottomTabNavigator } from '@react-navigation/bottom-tabs';
import Ionicons from 'react-native-vector-icons/Ionicons';

import Dashboard from '../screens/Dashboard/Dashboard'
import Constant from '../utilities/Constant';
import Explore from '../screens/Dashboard/Explore';
import PageUnderConstructions from '../screens/Utilities/PageUnderConstructions'

const Tab = createBottomTabNavigator();

export default function App() {
    return (<Tab.Navigator
        screenOptions={({ route }) => ({
            tabBarIcon: ({ focused, color, size }) => {
                let iconName;

                if (route.name === 'Dashboard') {
                    iconName = focused
                        ? 'ios-home'
                        : 'ios-home-outline';
                }
                else if (route.name === 'Explore') {
                    iconName = focused
                        ? 'ios-map'
                        : 'ios-map-outline';
                }
                else if (route.name === 'Cart') {
                    iconName = focused
                        ? 'ios-cart'
                        : 'ios-cart-outline';
                }
                else if (route.name === 'Notifications') {
                    iconName = focused
                        ? 'ios-notifications'
                        : 'ios-notifications-outline';
                }
                else if (route.name === 'Profile') {
                    iconName = focused
                        ? 'ios-person'
                        : 'ios-person-outline';
                }

                // You can return any component that you like here!
                return <Ionicons name={iconName} size={size} color={color} />;
            },
        })}
        tabBarOptions={{
            activeTintColor: Constant.Color.PRIMARY,
            inactiveTintColor: 'gray',
            showLabel: false,
            style: {
                height: 64
            }
        }}
    >
        <Tab.Screen name="Dashboard" component={Dashboard} />
        <Tab.Screen name="Explore" component={Explore} />
        <Tab.Screen name="Notifications" component={PageUnderConstructions} />
        <Tab.Screen name="Profile" component={PageUnderConstructions} />
    </Tab.Navigator >
    );
}