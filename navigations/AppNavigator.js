import * as React from 'react';
import { View, Text } from 'react-native';
import { NavigationContainer } from '@react-navigation/native';
import { createStackNavigator } from '@react-navigation/stack';

import Main from './TabNavigator';
import UniversalSearchPage from '../screens/Search/UniversalSearchPage';
import Cart from '../screens/Dashboard/Cart';

const Stack = createStackNavigator();

function App() {
    return (
        <NavigationContainer>
            <Stack.Navigator>
                <Stack.Screen
                    name="Main" component={Main}
                    options={{ headerShown: false }}
                />
                <Stack.Screen
                    name="UniversalSearchPage" component={UniversalSearchPage}
                    options={{ headerShown: false }}
                />
                <Stack.Screen
                    name="Cart" component={Cart}
                    options={{ headerShown: false }}
                />
            </Stack.Navigator>
        </NavigationContainer>
    );
}

export default App;