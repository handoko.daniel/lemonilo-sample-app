export default {
    Color: {
        PRIMARY: '#7dc243',
        ABSOLUTE_WHITE: '#FFFFFF',
        THIN_GRAY: '#C1C1C1',
        DARK_LABEL: '#444444'
    }
}