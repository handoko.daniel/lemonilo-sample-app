import React, { Component } from 'react';
import { View, Text, StyleSheet, ImageBackground, TouchableOpacity } from 'react-native';
import Constant from '../../../utilities/Constant';

export default class SearchResultCard extends Component {
    constructor(props) {
        super(props);
        this.state = {
        };
    }

    render() {
        return (
            <TouchableOpacity style={localStyles.container}>
                <View style={{ flex: 1, justifyContent: 'center', alignItems: 'center' }}>
                    <ImageBackground
                        source={{ uri: this.props.data.url }}
                        style={localStyles.image}
                    >

                    </ImageBackground>
                </View>
                <Text
                    numberOfLines={2}
                    style={localStyles.label}
                >{this.props.data.name}</Text>
                <Text style={localStyles.price}>Rp {this.props.data.price.toString().replace(/(.)(?=(\d{3})+$)/g, '$1.')}</Text>
                <TouchableOpacity
                    style={localStyles.button}
                >
                    <Text style={{ color: Constant.Color.ABSOLUTE_WHITE }}>Buy</Text>
                </TouchableOpacity>
            </TouchableOpacity>
        );
    }
}

const localStyles = StyleSheet.create({
    container: {
        borderWidth: 0.5,
        borderRadius: 16,
        padding: 16,
        overflow: "hidden",
        justifyContent: 'center',
    },
    label: {
        color: Constant.Color.DARK_LABEL,
        fontSize: 16,
        fontWeight: 'bold',
        marginVertical: 4
    },
    image: {
        width: "100%",
        height: 120,
        justifyContent: 'center',
        alignItems: 'center',
        resizeMode: 'contain'
    },
    price: {
        color: Constant.Color.DARK_LABEL,
        fontSize: 14,
        marginBottom: 8
    },
    button: {
        borderRadius: 8,
        width: "100%",
        padding: 8,
        justifyContent: 'center',
        alignItems: 'center',
        backgroundColor: Constant.Color.PRIMARY
    }
})