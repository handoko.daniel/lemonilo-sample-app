import React, { Component } from 'react';
import { View, Text, SafeAreaView, StyleSheet, ScrollView, TextInput } from 'react-native';
import { FlatGrid } from 'react-native-super-grid';
import Icon from 'react-native-vector-icons/Ionicons';

//import custom component
import StaticHeader from '../../components/general/StaticHeader';
import Constant from '../../utilities/Constant';

//import temporary data
import ProductData from '../Dashboard/data/products.json';
import SearchResultCard from './page_components/SearchResultCard';

export default class UniversalSearchPage extends Component {
    constructor(props) {
        super(props);
        let params = this.props.route.params.data.text != null ? this.props.route.params.data.text : ""
        this.state = {
            keyword: "",
            category: params,
            data: ProductData.data
        };
    }

    componentDidMount() {
        this.handleSearchProducts("")
    }

    handleRedirectUser(page) {
        if (page == "Back") {
            this.props.navigation.goBack()
        }
    }

    handleSearchProducts(value) {
        const { data, keyword, category } = this.state;
        if (category != "" && keyword == "") {
            let mutateDataState = [];
            data.forEach(item => {
                if (item.category.includes(category)) {
                    mutateDataState.push(item)
                }
            });
            this.setState({ keyword: value, data: mutateDataState, });
        }
        else if (value.toString() != "") {
            let mutateDataState = [];
            ProductData.data.forEach(item => {
                if (item.name.includes(keyword)) {
                    mutateDataState.push(item)
                }
            });
            this.setState({ keyword: value, data: mutateDataState });
        }
        else {
            this.setState({ keyword: value, data: ProductData.data, });
        }
    }

    render() {
        return (
            <SafeAreaView style={localStyles.container}>
                <StaticHeader
                    type="Back"
                    onPress={() => this.handleRedirectUser('Back')}
                />
                <View style={localStyles.searchInputContainer}>
                    <Icon
                        name="ios-search"
                        color={Constant.Color.THIN_GRAY}
                        style={{ marginRight: 16 }}
                        size={24}
                    />
                    <TextInput
                        value={this.state.keyword}
                        placeholder="Search.."
                        style={localStyles.input}
                        onChangeText={(value) => this.handleSearchProducts(value)}
                    />
                </View>
                <View style={{ flex: 1, backgroundColor: Constant.Color.ABSOLUTE_WHITE }}>
                    <FlatGrid
                        itemDimension={130}
                        data={this.state.data}
                        style={localStyles.gridView}
                        spacing={12}
                        renderItem={({ item }) => (
                            <SearchResultCard
                                data={item}
                            />
                        )}
                    />
                </View>
            </SafeAreaView>
        );
    }
}

const localStyles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: Constant.Color.PRIMARY
    },
    searchResult: {
        backgroundColor: Constant.Color.ABSOLUTE_WHITE
    },
    searchInputContainer: {
        backgroundColor: Constant.Color.ABSOLUTE_WHITE,
        width: "100%",
        flexDirection: 'row',
        alignItems: 'center',
        borderBottomWidth: 0.5,
        borderColor: Constant.Color.THIN_GRAY,
        padding: 16,
    },
    input: {
        backgroundColor: Constant.Color.ABSOLUTE_WHITE,
        width: "100%",
    },
    gridView: {
        flex: 1,
    },
    item: {
        backgroundColor: Constant.Color.ABSOLUTE_WHITE,
        justifyContent: 'flex-end',
        borderRadius: 5,
        padding: 8,
        height: 80,
        justifyContent: 'center',
        alignItems: 'center',
        shadowColor: "#000",
        shadowOffset: {
            width: 0,
            height: 2,
        },
        shadowOpacity: 0.25,
        shadowRadius: 3.84,
        elevation: 6,
        borderWidth: Platform.OS === "android" ? 0.3 : 0
    },
})