import React, { Component } from 'react';
import { StyleSheet, View, Text, Image, Platform } from 'react-native';
import { TouchableOpacity } from 'react-native-gesture-handler';
import { FlatGrid } from 'react-native-super-grid';
import Constant from '../../utilities/Constant';
import ProductCard from './page_components/ProductCard';


export default class Products extends Component {
    constructor(props) {
        super(props);
        this.state = {
        };
    }

    handleRedirectUser(page, data) {

    }

    handleOnProductPressed(data) {
        this.props.onBuyPressed(data)
    }

    render() {
        return (
            <View style={localStyles.container}>
                <Text style={localStyles.subTitle}>Best Seller</Text>
                <FlatGrid
                    itemDimension={130}
                    data={this.props.data}
                    style={localStyles.gridView}
                    spacing={12}
                    renderItem={({ item }) => (
                        <ProductCard
                            data={item}
                            key={item.id}
                            onBuyPressed={() => this.handleOnProductPressed(item)}
                        />
                    )}
                />
            </View>
        );
    }
}


const localStyles = StyleSheet.create({
    container: {
        borderTopWidth: 0.5,
        borderBottomWidth: 0.5,
        borderColor: Constant.Color.THIN_GRAY,
        paddingTop: 16,
    },
    subTitle: {
        fontStyle: 'italic',
        marginLeft: 16,
        fontSize: 16,
        fontWeight: 'bold'
    },
    gridView: {
        flex: 1,
    },
    item: {
        backgroundColor: Constant.Color.ABSOLUTE_WHITE,
        justifyContent: 'flex-end',
        borderRadius: 5,
        padding: 8,
        height: 80,
        justifyContent: 'center',
        alignItems: 'center',
        shadowColor: "#000",
        shadowOffset: {
            width: 0,
            height: 2,
        },
        shadowOpacity: 0.25,
        shadowRadius: 3.84,
        elevation: 6,
        borderWidth: Platform.OS === "android" ? 0.3 : 0
    },
    label: {
        color: Constant.Color.DARK_LABEL
    },
    icon: {
        width: 32,
        height: 32,
        marginBottom: 8
    }
});