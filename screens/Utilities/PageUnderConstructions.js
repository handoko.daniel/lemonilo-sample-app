import React, { Component } from 'react';
import { View, Text, StyleSheet, Image, Dimensions, StatusBar, SafeAreaView } from 'react-native';
import StaticHeader from '../../components/general/StaticHeader';
import Constant from '../../utilities/Constant';

export default class PageUnderConstructions extends Component {
    constructor(props) {
        super(props);
        this.state = {
        };
    }

    render() {
        return (
            <SafeAreaView style={localStyles.container}>
                <StatusBar
                    backgroundColor={Constant.Color.PRIMARY}
                />
                <StaticHeader />
                <View style={localStyles.body}>
                    <Image
                        source={require('../../assets/under_constructions.png')}
                        style={localStyles.image}
                    />
                    <Text style={localStyles.title}>This page is under constructions</Text>
                </View>
            </SafeAreaView>
        );
    }
}

const localStyles = StyleSheet.create({
    container: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center',
        backgroundColor: Constant.Color.PRIMARY
    },
    body: {
        flex: 1,
        backgroundColor: Constant.Color.ABSOLUTE_WHITE,
        width: "100%",
        alignItems: 'center',
    },
    image: {
        width: Dimensions.get('window').width * 0.8,
        height: Dimensions.get('window').width * 0.8,
        resizeMode: 'contain',
        borderRadius: (Dimensions.get('window').width * 0.8) / 2,
        overflow: 'hidden',
        marginBottom: 16
    },
    title: {
        fontWeight: 'bold',
        fontSize: 14,
        color: Constant.Color.DARK_LABEL,
        textAlign: 'center'
    }
})
