import React, { Component } from 'react';
import { View, Text, StyleSheet, Dimensions, Image } from 'react-native';
import { ScrollView } from 'react-native-gesture-handler';

export default class Carousel extends Component {
    constructor(props) {
        super(props);
        this.state = {
        };
    }

    renderImage() {
        const { data } = this.props;
        return (
            data.map((item, index) =>
                <View
                    style={localStyles.imageContainer}
                    key={index}
                >
                    <Image
                        source={{ uri: item.url }}
                        style={localStyles.image}
                    />
                </View>
            )
        )
    }

    render() {
        return (
            <View style={localStyles.container}>
                <Text style={localStyles.subTitle}>Promotions</Text>
                <ScrollView
                    pagingEnabled
                    horizontal
                    showsHorizontalScrollIndicator={false}
                >
                    {
                        this.renderImage()
                    }
                </ScrollView>
            </View>
        );
    }
}

const localStyles = StyleSheet.create({
    container: {
        paddingVertical: 16
    },
    subTitle: {
        fontStyle: 'italic',
        marginLeft: 16,
        marginBottom: 16,
        fontSize: 16,
        fontWeight: 'bold'
    },
    imageContainer: {
        width: Dimensions.get('window').width,
        height: Dimensions.get('window').width * 0.6,
        justifyContent: 'center',
        alignItems: 'center'
    },
    image: {
        width: Dimensions.get('window').width - 32,
        height: Dimensions.get('window').width * 0.6,
        borderRadius: 16,
        overflow: 'hidden'
    }
})