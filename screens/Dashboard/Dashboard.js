import React, { Component } from 'react';
import { View, Text, SafeAreaView, StyleSheet, StatusBar, ScrollView, Image, Dimensions, FlatList, TouchableOpacity, TextInput } from 'react-native';
import Modal from 'react-native-modal';
import { connect } from 'react-redux';

//import custom component
import StaticHeader from '../../components/general/StaticHeader';
import Constant from '../../utilities/Constant';
import Carousel from './page_components/Carousel';
import Categories from './page_components/Categories';
import Products from '../Products/Products';
import { addToCart, setInitialValue } from '../../redux/actions';


//import temporary data
import Banner from './data/banner.json';
import ProductData from './data/products.json';

class Dashboard extends Component {
    constructor(props) {
        super(props);
        this.state = {
            banner: Banner,
            products: ProductData,
            categories: [
                {
                    url: require('../../assets/broccoli.png'),
                    text: 'Vegetables',
                },
                {
                    url: require('../../assets/apple.png'),
                    text: 'Fruits'
                },
                {
                    url: require('../../assets/juice.png'),
                    text: 'Juices'
                },
                {
                    url: require('../../assets/bread.png'),
                    text: 'Breads'
                },
                {
                    url: require('../../assets/cutlery.png'),
                    text: 'Cutlery'
                }
            ],
            modalStatus: false,
            selectedProduct: null,
            quantity: 0,
            userCart: []
        };
    }

    componentDidMount() {
        this.props.dispatch(setInitialValue())
    }

    handleRedirectUser(page) {
        this.props.navigation.navigate(page, { data: "" })
    }

    handleOnBuyPress = (selectedProduct) => {
        this.setState({ modalStatus: true, selectedProduct });
    }

    handleQuantity(action) {
        const { quantity } = this.state;

        if (action == "minus" && quantity > 0) {
            this.setState({ quantity: quantity - 1 })
        }
        else if (action == "plus") {
            this.setState({ quantity: quantity + 1 })
        }
        else {
            this.setState({ quantity: 0 })
        }
    }

    handleAddToCart() {
        const { selectedProduct, userCart, quantity } = this.state;
        const { cart } = this.props;
        if (cart.length > 0) {
            let duplicateProductIndex = -1;
            for (let index = 0; index < cart.length; index++) {
                if (cart[index].name == selectedProduct.name) {
                    duplicateProductIndex = index;
                }
            }

            if (duplicateProductIndex >= 0) {
                cart[duplicateProductIndex].quantity = parseInt(cart[duplicateProductIndex].quantity) + quantity;
            }
            else {
                selectedProduct.quantity = quantity;
                cart.push(selectedProduct);
            }
            
            this.props.dispatch(addToCart(cart));
            this.setState({ modalStatus: false, quantity: 0 });
        }
        else {
            if (quantity > 0) {
                selectedProduct.quantity = quantity;
                cart.push(selectedProduct)
                this.props.dispatch(addToCart(cart));
                this.setState({ modalStatus: false, quantity: 0 });
            }
        }
    }

    renderModal() {
        const { selectedProduct } = this.state;
        if (selectedProduct != null) {
            return (
                <View style={localStyles.modal}>
                    <View style={{ flexDirection: 'row' }}>
                        <Image
                            source={{ uri: selectedProduct.url }}
                            style={{ width: 100, height: 100 }}
                        />
                        <View style={{ flex: 1, paddingLeft: 16, justifyContent: 'center', }}>
                            <Text style={localStyles.modalItemName}>{selectedProduct.name}</Text>
                            <Text style={localStyles.modalItemName}>Rp {selectedProduct.price.toString().replace(/(.)(?=(\d{3})+$)/g, '$1.')}</Text>
                        </View>
                    </View>
                    <View style={{ flexDirection: 'row', justifyContent: 'center' }}>
                        <TouchableOpacity
                            onPress={() => this.handleQuantity('minus')}
                            style={[localStyles.modalButton, { marginRight: 8 }]}
                        >
                            <Text style={{ fontSize: 16, fontWeight: 'bold' }}>-</Text>
                        </TouchableOpacity>
                        <TextInput
                            style={{ flex: 1, borderBottomWidth: 0.5 }}
                            placeholder="Quantity"
                            value={this.state.quantity.toString()}
                        />
                        <TouchableOpacity
                            onPress={() => this.handleQuantity('plus')}
                            style={[localStyles.modalButton, { marginLeft: 8 }]}
                        >
                            <Text style={{ fontSize: 16, fontWeight: 'bold' }}>+</Text>
                        </TouchableOpacity>
                    </View>
                    <TouchableOpacity
                        style={localStyles.submitButton}
                        onPress={() => this.handleAddToCart()}
                    >
                        <Text style={{ color: Constant.Color.ABSOLUTE_WHITE }}>Buy</Text>
                    </TouchableOpacity>
                </View>
            )
        }
        else {
            return (<View />)
        }
    }

    render() {
        console.log(this.props.cart);
        return (
            <SafeAreaView style={localStyles.container}>
                <StatusBar
                    backgroundColor={Constant.Color.PRIMARY}
                    barStyle="light-content"
                />
                <StaticHeader
                    type="default"
                    onPress={() => this.handleRedirectUser('UniversalSearchPage')}
                    onCartPressed={() => this.handleRedirectUser('Cart')}
                />
                <FlatList
                    style={localStyles.body}
                    showsVerticalScrollIndicator={false}
                    ListHeaderComponent={
                        <>
                            <Carousel
                                data={this.state.banner.data}
                            />
                            <Categories
                                data={this.state.categories}
                                navigation={this.props.navigation}
                            />
                            <Products
                                data={this.state.products.data}
                                onBuyPressed={this.handleOnBuyPress}
                            />
                        </>
                    }
                />
                <Modal
                    isVisible={this.state.modalStatus}
                    animationIn="bounceIn"
                    animationOut="bounceOut"
                    animationInTiming={1000}
                    animationOutTiming={1000}
                    onBackdropPress={() => this.setState({ modalStatus: false })}
                    style={{
                        justifyContent: 'center',
                        alignItems: 'center',
                        padding: 10
                    }}
                >
                    {this.renderModal()}
                </Modal >
            </SafeAreaView>
        );
    }
}

const localStyles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: Constant.Color.PRIMARY
    },
    body: {
        backgroundColor: Constant.Color.ABSOLUTE_WHITE
    },
    modal: {
        width: Dimensions.get('window').width * 0.8,
        backgroundColor: Constant.Color.ABSOLUTE_WHITE,
        borderRadius: 16,
        padding: 16
    },
    modalItemName: {
        fontWeight: "bold",
        marginBottom: 8,
        fontSize: 16
    },
    modalButton: {
        width: 36,
        height: 36,
        justifyContent: 'center',
        alignItems: 'center',
        borderWidth: 1,
        borderColor: Constant.Color.PRIMARY,
        borderRadius: 18
    },
    submitButton: {
        width: "100%",
        padding: 16,
        justifyContent: 'center',
        alignItems: "center",
        backgroundColor: Constant.Color.PRIMARY,
        marginTop: 16,
        borderRadius: 16
    }
})

const mapStateToProps = (state) => {
    return { cart: state.cart };
};

export default connect(mapStateToProps)(Dashboard);
