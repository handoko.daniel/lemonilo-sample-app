import React, { Component } from 'react';
import { View, Text, StyleSheet, SafeAreaView, StatusBar, ScrollView, Image, TouchableOpacity } from 'react-native';
import { connect } from 'react-redux'
import StaticHeader from '../../components/general/StaticHeader';
import Constant from '../../utilities/Constant';

class Cart extends Component {
    constructor(props) {
        super(props);
        this.state = {
        };
    }

    handleRedirectUser(page) {
        if (page == "Back") {
            this.props.navigation.goBack();
        }
    }

    renderCart() {
        const { cart } = this.props;
        return (
            cart.map((item, index) =>
                <View
                    style={localStyles.cardContainer}
                    key={index}
                >
                    <Image
                        source={{ uri: item.url }}
                        style={{ width: 80, height: 80 }}
                    />
                    <View style={{ flex: 1, paddingLeft: 8 }}>
                        <Text style={localStyles.title}>{item.name}</Text>
                        <Text style={localStyles.label}>{parseInt(item.quantity) + " x Rp " + item.price.toString().replace(/(.)(?=(\d{3})+$)/g, '$1.')}</Text>
                    </View>
                </View>
            )
        )
    }

    renderPrice() {
        const { cart } = this.props;
        let subtotal = 0;

        cart.map((item, index) =>
            subtotal = subtotal + (item.quantity * item.price)
        )
        return (subtotal.toString().replace(/(.)(?=(\d{3})+$)/g, '$1.'));
    }

    render() {
        return (
            <SafeAreaView style={localStyles.container}>
                <StatusBar
                    backgroundColor={Constant.Color.PRIMARY}
                    barStyle="light-content"
                />
                <StaticHeader
                    type="Back"
                    onPress={() => this.handleRedirectUser('Back')}
                />
                <ScrollView style={localStyles.body}>
                    {this.renderCart()}
                </ScrollView>
                <View style={{ width: "100%", paddingVertical: 16, flexDirection: 'row', paddingHorizontal: 16, backgroundColor: Constant.Color.ABSOLUTE_WHITE }}>
                    <View style={{ flex: 1 }}>
                        <Text style={{ fontSize: 24 }}>Total</Text>
                    </View>
                    <View style={{ flex: 1, alignItems: 'flex-end' }}>
                        <Text style={{ fontSize: 24 }}>Rp {this.renderPrice()}</Text>
                    </View>
                </View>
                <TouchableOpacity
                    style={localStyles.payment}
                >
                    <Text style={{ fontWeight: 'bold', color: Constant.Color.ABSOLUTE_WHITE, fontSize: 16 }}>Continue to Payment</Text>
                </TouchableOpacity>
            </SafeAreaView>
        );
    }
}

const localStyles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: Constant.Color.PRIMARY
    },
    body: {
        backgroundColor: Constant.Color.ABSOLUTE_WHITE
    },
    title: {
        fontSize: 16,
        fontWeight: 'bold',
        marginBottom: 8
    },
    label: {
        fontSize: 14,
    },
    cardContainer: {
        borderBottomWidth: 0.5,
        padding: 16,
        flexDirection: 'row',
        justifyContent: 'center',
        alignItems: 'center'
    },
    payment: {
        height: 64,
        justifyContent: 'center',
        alignItems: 'center'
    }
})


const mapStateToProps = (state) => {
    return { cart: state.cart };
};

export default connect(mapStateToProps)(Cart);