import React, { Component } from 'react';
import { SafeAreaView, StatusBar, FlatList, StyleSheet } from 'react-native';
import StaticHeader from '../../components/general/StaticHeader';
import Constant from '../../utilities/Constant';
import News from '../News/News';
import Informations from './data/news.json';

export default class Explore extends Component {
    constructor(props) {
        super(props);
        this.state = {
            news: Informations,
        };
    }

    handleRedirectUser(page) {
        this.props.navigation.navigate(page)
    }

    render() {
        return (
            <SafeAreaView style={localStyles.container}>
                <StatusBar
                    backgroundColor={Constant.Color.PRIMARY}
                    barStyle="light-content"
                />
                <StaticHeader
                    type="default"
                    onPress={() => this.handleRedirectUser('UniversalSearchPage')}
                />
                <FlatList
                    style={localStyles.body}
                    ListHeaderComponent={
                        <>
                            <News
                                data={this.state.news.data}
                            />
                        </>
                    }
                />
            </SafeAreaView>
        );
    }
}

const localStyles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: Constant.Color.PRIMARY
    },
    body: {
        flex: 1,
        backgroundColor: Constant.Color.ABSOLUTE_WHITE
    },
})