import React, { Component } from 'react';
import { View, Text, StyleSheet, Image, Dimensions } from 'react-native';
import Constant from '../../../utilities/Constant';

export default class NewsCard extends Component {
    constructor(props) {
        super(props);
        this.state = {
        };
    }

    render() {
        return (
            <View style={localStyles.container}>
                <Image
                    source={{ uri: this.props.data.url }}
                    style={localStyles.image}
                />
                <View style={{ flex: 1, paddingLeft: 16 }}>
                    <Text style={localStyles.title}>{this.props.data.title}</Text>
                    <Text
                        style={localStyles.description}
                        numberOfLines={5}
                    >{this.props.data.description}</Text>
                </View>
            </View>
        );
    }
}

const localStyles = StyleSheet.create({
    container: {
        backgroundColor: Constant.Color.ABSOLUTE_WHITE,
        width: "100%",
        padding: 16,
        flexDirection: 'row',
        shadowColor: "#000",
        shadowOffset: {
            width: 0,
            height: 3,
        },
        shadowOpacity: 0.27,
        shadowRadius: 4.65,
        borderRadius: 16,
        elevation: 6,
        marginBottom: 8
    },
    image: {
        width: Dimensions.get('window').width * 0.3,
        height: Dimensions.get('window').width * 0.3,
    },
    title: {
        fontWeight: 'bold',
        fontSize: 16,
        marginBottom: 8
    },
    description: {
        lineHeight: 24,
        fontSize: 14
    }
})