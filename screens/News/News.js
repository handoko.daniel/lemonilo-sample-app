import React, { Component } from 'react';
import { View, Text, StyleSheet, Dimensions, Image } from 'react-native';
import { ScrollView } from 'react-native-gesture-handler';
import NewsCard from './page_components/NewsCard';

export default class News extends Component {
    constructor(props) {
        super(props);
        this.state = {
        };
    }

    renderNews() {
        const { data } = this.props;
        return (
            data.map((item, index) =>
                <NewsCard
                    key={index}
                    data={item}
                />
            )
        )
    }

    render() {
        return (
            <View style={localStyles.container}>
                <Text style={localStyles.subTitle}>Top picks for your</Text>
                {this.renderNews()}
            </View>
        );
    }
}

const localStyles = StyleSheet.create({
    container: {
        paddingHorizontal: 16,
        marginTop: 16,
    },
    subTitle: {
        fontStyle: 'italic',
        marginBottom: 16,
        fontSize: 16,
        fontWeight: 'bold'
    },
    imageContainer: {
        width: Dimensions.get('window').width,
        height: Dimensions.get('window').width * 0.6,
        justifyContent: 'center',
        alignItems: 'center'
    },
    image: {
        width: Dimensions.get('window').width - 32,
        height: Dimensions.get('window').width * 0.6,
        borderRadius: 16,
        overflow: 'hidden'
    }
})