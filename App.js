import React from 'react';
import AppNavigator from './navigations/AppNavigator';
import AsyncStorage from '@react-native-async-storage/async-storage';
import { Provider } from 'react-redux';

import { persistStore, persistReducer } from 'redux-persist';
import { compose, createStore, applyMiddleware } from 'redux';
import reducers from './redux/reducers';
import thunkMiddleWare from 'redux-thunk';
import logger from 'redux-logger';

const enhancers = window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__ || compose; // eslint-disable-line
const persistConfig = {
    key: 'root',
    storage: AsyncStorage
};
const pReducer = persistReducer(persistConfig, reducers);

export const store = createStore(pReducer, {}, enhancers(applyMiddleware(thunkMiddleWare, logger)));
export const persistor = persistStore(store);
export default function App() {
    return (
        <Provider
            store={store}
        >
            <AppNavigator />
        </Provider>
    );
}